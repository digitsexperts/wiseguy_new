class AddReportImageToPublishers < ActiveRecord::Migration
   def change
    add_column :publishers, :report_image, :string
  end
end
