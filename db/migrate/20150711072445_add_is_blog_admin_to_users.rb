class AddIsBlogAdminToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_blog_admin, :boolean
  end
end
