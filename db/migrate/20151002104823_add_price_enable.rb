class AddPriceEnable < ActiveRecord::Migration
  def change
  	add_column :publishers, :report_price_flag, :boolean, :default => true
  end
end
