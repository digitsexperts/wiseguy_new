class AddPurchaseTypeToShoppingCartItems < ActiveRecord::Migration
  def change
    add_column :shopping_cart_items, :purchase_type, :string
  end
end
