class AddSampleReportsEnabledFlag < ActiveRecord::Migration
  def change
  	add_column :publishers, :sample_report_flag, :boolean, :default => true
  end
end
