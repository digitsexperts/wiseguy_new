class AddReportIdToEnquiry < ActiveRecord::Migration
  def change
    add_column :enquiries, :report_id, :integer
  end
end
