class AddReportPdfToReports < ActiveRecord::Migration
  def change
    add_column :reports, :report_pdf, :string
  end
end
