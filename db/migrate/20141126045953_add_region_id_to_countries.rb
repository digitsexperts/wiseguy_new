class AddRegionIdToCountries < ActiveRecord::Migration
  def change
    add_column :countries, :region_ids, :text
  end
end
