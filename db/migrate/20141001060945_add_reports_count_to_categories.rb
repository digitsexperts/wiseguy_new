class AddReportsCountToCategories < ActiveRecord::Migration
  def up
    add_column :categories, :reports_count, :integer, :default => 0

	  Category.reset_column_information
	  Category.all.each do |p|
	    Category.update_counters p.id, :reports_count => p.reports.length
	  end
  end

  def down
    remove_column :categories, :reports_count
  end
end
