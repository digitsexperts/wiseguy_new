class AddOmniauthDataToUsers < ActiveRecord::Migration
  def change
    add_column :users, :omniauth_data, :text
  end
end
