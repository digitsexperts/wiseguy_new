class AddDeltaToCountries < ActiveRecord::Migration
  def change
  	add_column :countries, :delta, :boolean, :default => true
  end
end
