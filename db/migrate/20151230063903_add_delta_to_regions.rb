class AddDeltaToRegions < ActiveRecord::Migration
  def change
  	add_column :regions, :delta, :boolean, :default => true
  end
end
