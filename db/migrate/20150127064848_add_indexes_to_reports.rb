class AddIndexesToReports < ActiveRecord::Migration
  def change
  	add_index :reports, :publisher_id
    add_index :reports, :category_id
    add_index :reports , :region_id
    add_index :reports , :continent_id
  end
end
