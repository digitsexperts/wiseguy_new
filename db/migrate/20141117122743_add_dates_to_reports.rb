class AddDatesToReports < ActiveRecord::Migration
  def change
    add_column :reports, :day, :string
    add_column :reports, :publish_month, :string
  end
end
