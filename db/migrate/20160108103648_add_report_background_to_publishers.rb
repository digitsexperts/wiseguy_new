class AddReportBackgroundToPublishers < ActiveRecord::Migration
  def change
    add_column :publishers, :report_background, :string
  end
end
