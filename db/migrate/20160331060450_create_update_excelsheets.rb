class CreateUpdateExcelsheets < ActiveRecord::Migration
  def change
    create_table :update_excelsheets do |t|
      t.string :file
      t.integer :status
      t.timestamps
    end
  end
end
