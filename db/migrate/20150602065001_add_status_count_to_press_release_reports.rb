class AddStatusCountToPressReleaseReports < ActiveRecord::Migration
  def change
    add_column :press_release_reports, :success, :string
    add_column :press_release_reports, :failed, :string
  end
end
