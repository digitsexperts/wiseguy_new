class AddSlugToNewUpdates < ActiveRecord::Migration
  def change
    add_column :news_updates, :slug, :text
  end
end
