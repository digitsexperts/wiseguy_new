class CreateInfoGraphics < ActiveRecord::Migration
  def change
    create_table :info_graphics do |t|
      t.string :report_image

      t.timestamps
    end
  end
end
