class AddRegionIdsToReports < ActiveRecord::Migration
   def change
    add_column :reports, :region_id, :integer
    add_column :reports, :continent_id, :integer
  end
end
