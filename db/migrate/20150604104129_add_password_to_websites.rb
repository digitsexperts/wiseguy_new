class AddPasswordToWebsites < ActiveRecord::Migration
  def change
    add_column :websites, :username, :string
    add_column :websites, :password, :string
  end
end
