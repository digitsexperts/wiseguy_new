class AddDeltaToPublishers < ActiveRecord::Migration
  def change
  	add_column :publishers, :delta, :boolean, :default => true
  end
end
