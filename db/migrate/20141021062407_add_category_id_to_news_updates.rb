class AddCategoryIdToNewsUpdates < ActiveRecord::Migration
  def change
    add_column :news_updates, :category_id, :integer
  end
end
