class AddLogoToPublishers < ActiveRecord::Migration
  def change
    add_column :publishers, :logo, :string
  end
end
