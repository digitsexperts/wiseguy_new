class AddTextPositionToPublishers < ActiveRecord::Migration
  def change
    add_column :publishers, :text_position, :string
    add_column :publishers, :text_color, :string
  end
end
