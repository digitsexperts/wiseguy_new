class AddReportsCountToContinent < ActiveRecord::Migration
  def change
    add_column :continents, :reports_count, :integer
  end
end
