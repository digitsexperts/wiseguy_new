class CreateBlogsComments < ActiveRecord::Migration
  def change
    create_table :blogs_comments do |t|
      t.string :name
      t.string :email
      t.text :message
      t.integer :blog_id

      t.timestamps
    end
  end
end
