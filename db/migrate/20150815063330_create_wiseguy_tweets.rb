class CreateWiseguyTweets < ActiveRecord::Migration
  def change
    create_table :wiseguy_tweets do |t|
      t.string :user_screen_name
      t.text :tweet_text
      t.datetime :tweet_created_at
      t.string :tweet_id

      t.timestamps
    end
  end
end
