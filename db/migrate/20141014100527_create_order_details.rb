class CreateOrderDetails < ActiveRecord::Migration
  def change
    create_table :order_details do |t|
      t.integer  :order_id
      t.string   :order_details_status
      t.integer  :report_id
      t.string   :report_value
      t.integer  :quantity
      t.timestamps
    end
  end
end
