class AddIndexToReports < ActiveRecord::Migration
  def change
  	add_index "reports", ["one_user"], name: "index_one_user", using: :btree
  	add_index "reports", ["five_user"], name: "index_five_user", using: :btree
  	add_index "reports", ["site_user"], name: "index_site_user", using: :btree
  	add_index "reports", ["enterprise_user"], name: "index_enterprise_user", using: :btree
  end
end
