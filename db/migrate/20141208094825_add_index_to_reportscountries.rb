class AddIndexToReportscountries < ActiveRecord::Migration
  def change
  	add_index :countries_reports, [:country_id, :report_id]
    add_index :countries_reports, [:report_id, :country_id]
  end
end
