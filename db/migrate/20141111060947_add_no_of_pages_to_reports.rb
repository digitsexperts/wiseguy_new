class AddNoOfPagesToReports < ActiveRecord::Migration
  def change
    add_column :reports, :no_of_pages, :integer
  end
end
