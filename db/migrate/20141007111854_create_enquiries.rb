class CreateEnquiries < ActiveRecord::Migration
  def change
    create_table :enquiries do |t|
      t.string   :first_name
      t.string   :last_name
      t.string   :email
      t.string   :job_title
      t.string   :phone_no
      t.text     :interest_area
      t.text     :enquiry
      t.text     :message
      t.string   :enquiry_type
      t.timestamps
    end
  end
end
