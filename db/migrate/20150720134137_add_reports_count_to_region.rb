class AddReportsCountToRegion < ActiveRecord::Migration
  def change
    add_column :regions, :reports_count, :integer
  end
end
