class AddColumnsToSubscribers < ActiveRecord::Migration
  def change
    add_column :subscribers, :status, :boolean
    add_column :subscribers, :reason, :text
  end
end
