class AddEditionColumnsToReports < ActiveRecord::Migration
  def change
  	    add_column :reports, :one_user, :float
    add_column :reports, :five_user, :float
    add_column :reports, :site_user, :float
    add_column :reports, :enterprise_user, :float
  end
end
