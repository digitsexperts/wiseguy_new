class AddChangesToReports < ActiveRecord::Migration
  def change
  	change_column :reports, :table_of_content, :text, :limit => 4294967295
  end
end
