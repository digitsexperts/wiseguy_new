class CreateAdminBlogs < ActiveRecord::Migration
  def change
    create_table :admin_blogs do |t|
      t.text :title
      t.text :content
      t.integer :user_id
      t.integer :category_id

      t.timestamps
    end
  end
end
