class AddDiscountedPriceToShoppingCartItems < ActiveRecord::Migration
  def change
    add_column :shopping_cart_items, :discounted_price, :decimal
  end
end
