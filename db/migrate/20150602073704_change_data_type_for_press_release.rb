class ChangeDataTypeForPressRelease < ActiveRecord::Migration
  def change
  	change_column :press_release_reports, :success, :text
  	change_column :press_release_reports, :failed, :text
  end
end
