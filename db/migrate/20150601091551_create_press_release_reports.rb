class CreatePressReleaseReports < ActiveRecord::Migration
  def change
    create_table :press_release_reports do |t|
      t.integer  :website_id
      t.integer  :report_id
      t.string   :status

      t.timestamps
    end
  end
end
