class CreateReports < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.string      :title
      t.text        :description
      t.text        :table_of_content
      t.date        :publish_date
      t.float       :price
      t.integer     :publisher_id
      t.integer     :category_id
      t.integer     :country_id
      t.timestamps
    end
  end
end
