class AddNewsImageToNewsUpdates < ActiveRecord::Migration
  def change
    add_column :news_updates, :news_image, :string
  end
end
