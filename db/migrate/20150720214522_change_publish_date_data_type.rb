class ChangePublishDateDataType < ActiveRecord::Migration
  def change
  	change_column :reports, :publish_date, :integer
  end
end
