# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
 puts "START SEEDING"
 Continent.create([{id: 1, name: "Africa",slug: "africa"}, {id: 2, name: "Asia",slug: "asia"}, {id: 3,name: "Australia",slug: "australia"}, {id: 4,name: "Europe",slug: "europe"},{id: 5,name: "North America", slug: "north-america"}, {id: 6,name: "South America", slug: "south-america"}])

 Rate.create([{id: 1, currency: "gbp", exchange_rate: 0.6715},{id: 2, currency: "eur", exchange_rate: 0.9202}, {id: 3, currency: "yen", exchange_rate: 121.14}, {id: 4, currency: "inr", exchange_rate: 66.3134}])

 Region.create([{id: 1,  name: "Bric",slug: "bric"},
				{id: 2,  name: "CIS",slug: "cis"},
				{id: 3,  name: "European Union",slug: "european-union"},
				{id: 4,  name: "G20",slug: "g20"},
				{id: 5,  name: "G8", slug: "g8"},
				{id: 6,  name: "NAFTA", slug: "nafta"},
				{id: 7,  name: "Asia Pacific", slug: "asia-pacific"},
				{id: 8,  name: "Caribbean", slug: "caribbean"},
				{id: 9,  name: "Central America", slug: "central-america"},
				{id: 10, name: "Central Asia", slug: "central-asia"},
				{id: 11, name: "Central Europe", slug: "central-europe"},
				{id: 12, name: "East Africa", slug: "east-africa"},
				{id: 13, name: "East Europe", slug: "east-europe"},
				{id: 14, name: "Latin America", slug: "latin-america"},
				{id: 15, name: "Micronesia", slug: "micronesia"},
				{id: 16, name: "Middle Africa", slug: "middle-africa"},
				{id: 17, name: "Middle East", slug: "middle-east"},
				{id: 18, name: "Northern Africa", slug: "northern-africa"},
				{id: 19, name: "Northern Europe", slug: "northern-europe"},
				{id: 20, name: "Oceania", slug: "oceania"},
				{id: 21, name: "Scandinavia", slug: "scandinavia"},
				{id: 22, name: "South Asia", slug: "south-asia"},
				{id: 23, name: "South Europe", slug: "south-europe"},
				{id: 24, name: "Southeast Asia", slug: "southeast-asia"},
				{id: 25, name: "Southern Africa", slug: "southern-africa"},
				{id: 26, name: "West Africa", slug: "west-africa"},
				{id: 27, name: "West Europe", slug: "west-europe"}

				])




 file_to_load  = Rails.root + 'db/seed/countries.yml'
 countries_list   = YAML::load( File.open( file_to_load ) )

 countries_list.each_pair do |key,country|
  s = Country.find_by_abbreviation(country['abbreviation'])
  unless s
     c = Country.create(country) unless s
  #    if c.name.match(/\s/)
  #      c.update_attribute(:flag_image, "http://www.printableworldflags.com/icon-flags/32/#{c.name.gsub!(' ', '%20')}.png") 
	 # else
	 #   c.update_attribute(:flag_image, "http://www.printableworldflags.com/icon-flags/32/#{c.name}.png") 
  #    end
 end
 end