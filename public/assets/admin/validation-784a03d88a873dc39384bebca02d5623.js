jQuery(document).ready(function(){jQuery("#new_category").validate({errorElement:"div",rules:{"category[name]":{required:!0,maxlength:100}},messages:{"category[name]":{required:"Please enter category name"}}}),jQuery("#new_publisher").validate({errorElement:"div",rules:{"publisher[name]":{required:!0,maxlength:100}},messages:{"publisher[name]":{required:"Please enter publisher name"}}}),jQuery("#new_news_update").validate({errorElement:"div",rules:{"news_update[title]":{required:!0},"news_update[description]":{required:!0}},messages:{"news_update[title]":{required:"Please enter news title"},"news_update[description]":{required:"Please enter news description"}}}),jQuery("#new_report").validate({errorElement:"div",rules:{"report[title]":{required:!0},"report[description]":{required:!0},"report[price]":{required:!0},"report[publisher_id]":{required:!0},"report[category_id]":{required:!0},"report[country_id]":{required:!0}},messages:{"report[title]":{required:"Please enter report title"},"report[description]":{required:"Please enter report description"},"report[price]":{required:"Please enter report price"},"report[publisher_id]":{required:"Please select report publisher"},"report[category_id]":{required:"Please select report category"},"report[country_id]":{required:"Please select report country"}}})});