# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
# #
#   set :environment, 'development'



# Learn more: http://github.com/javan/whenever
root = File.expand_path("..",File.dirname(__FILE__))

  set :output, {
 :error    => "#{root}/log/error.log",
 :standard => "#{root}/log/cron.log"
  }

  # every 1.day, :at => '10:00 pm' do
  #  runner "Report.send_emails"
  # end
  job_type :runner, "cd /home/ubuntu/www/wiseguy_optimized && RAILS_ENV=production rails runner ':task' >> /home/ubuntu/www/wiseguy_optimized/log/error.log"


  every 10.minutes do
   runner "Excelsheet.dump_excelsheets"
  end

  # every 15.minutes do
  #  runner "CronController.index_wiseguy_main_db"
  # end

  # every 25.minutes do
  #  runner "CronController.set_local_indexing_need"
  # end

  every 1.day, :at => '5:00 am' do
   runner "Rate.get_rate"
  end

  every 6.hours do
   runner 'WiseguyTweet.fetch_latest_tweets'
  end
#
# every 15.minutes do
#    runner "PressReleaseReport.dump_and_send_for_processing"
# end

# Learn more: http://github.com/javan/whenever



