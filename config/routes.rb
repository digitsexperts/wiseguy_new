Rails.application.routes.draw do




  match "/report_back", :to => "reports#lolcat", :via => [:get]
  post '/tinymce_assets' => 'tinymce_assets#create'
  
  namespace :admin do
    resources :blog_rss_feeds
  end

  get "/rss-feeds" =>"home#rss_feeds"
  get "/rss/category/:id" => "home#get_rss_feed_of_category"
  get "/rss/publisher/:id" => "home#get_rss_feed_of_publisher"
  get "/rss/countries/:id" => "home#get_rss_feed_of_country"
  get "/rss/continents/:id" => "home#get_rss_feed_of_continents"
  get "/rss/regions/:id" => "home#get_rss_feed_of_regions"
  get "/rss/news-and-update" => "home#get_rss_feed_of_news_and_update"
  get "/rss/latest-reports" => "home#get_latest_reports"
  get "/rss/top-selling-reports" => "home#get_top_selling_reports"
  get "/rss/upcoming-reports" => "home#get_rss_feed_of_upcoming_reports"
  get "/rss/popular-categories" => "home#get_rss_feed_of_popular_categories"
  post "/show_side_panel_of_category" => "home#show_side_panel_of_category"

  #filter routes
  post "/show_all_publishers" => "report_filters#show_all_publishers"
  post "/show_default_publishers" => "report_filters#show_default_publishers"
  post "/show_filter_reports" => "report_filters#show_filter_reports"
  get "/get_all_publishers" => "report_filters#get_all_publishers"
  get "/sort_publishers/:name" =>"report_filters#sort_publishers"
  post "/show_all_countries" => "report_filters#show_all_countries"
  post "/show_all_categories" => "report_filters#show_all_categories" 
  post "/show_all_continents" => "report_filters#show_all_continents"
  post "/show_all_regions" => "report_filters#show_all_regions"
  post "/show_selected_regions" => "report_filters#show_selected_regions"
  post "/show_selected_continents" => "report_filters#show_selected_continents"
  post "/reset_to_default_by_category" => "categories#reset_to_default_by_category"
  post "/new_report_enquiry" => "home#new_report_enquiry"
  post "/hdfc_secret" => "shopping_carts#hdfc_secret"
  post "/user_detail" => "shopping_carts#user_detail"

  get "/reset_countries" => "report_filters#reset_countries"
  post "/advance_search_countries" => "report_filters#advance_search_countries"
  
  resources :blogs, only: [:show,:index] do
    get 'blog_comments'
    collection do
      match "/category/:slug", :to => "blogs#blog_category",  :via => [:get, :post], :as => :blog_category
      match 'search' => 'blogs#search', :via => [:get, :post], :as => :blog_search
    end
  end
  get 'tags/:tag', to: 'blogs#index', as: :tag

  
  match 'blogs/by_year_and_month/:year/:month' => 'blogs#by_year_and_month', :via => [:get, :post], :as=> :blogs_by_year_and_month

  
  resources :user_sessions, only: [:new,:create,:destroy]
  # post "/transaction/ccavRequestHandler" => "shopping_carts#ccavRequestHandler"
  
  post "/transaction/buynowccavRequestHandler" => "shopping_carts#buynowccavRequestHandler"
  match "/transaction/ccavRequestHandler" =>'shopping_carts#ccavRequestHandler', :via => [:get, :post]

  match "/transaction/ccavResponseHandler" =>'shopping_carts#ccavResponseHandler', :via => [:get, :post]
  match "/transaction/buynowCcavenueResponse" =>'shopping_carts#buynowCcavenueResponse', :via => [:get, :post]

  match "/transaction/cancel" =>'shopping_carts#cancel_transaction', :via => [:get, :post]
  resources :users, only: [:show,:new,:create]
  get 'signup' => 'users#new', :as => :signup

  root :to => 'home#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'
  get 'login'   => 'user_sessions#new', as: :login
  delete 'logout' => 'user_sessions#destroy', as: :logout
  get 'signout' => 'user_sessions#destroy', as: :signout
  # Example of regular route:
  resources :password_resets, :only => [:new, :edit, :update]
  resources :news_updates, :only => [:show,:index], :path => '/press-release'
  #   get 'products/:id' => 'catalog#view'
  namespace :admin do 
    resources :blogs do
      collection do
        get "blog_logs"
        get "blog_log_detail"
      end
    end
    match '/dashboard' => "dashboard#index", :via => ["get"], :as => :root
    match "/blogs/:id/delete", :to => "blogs#destroy", :via => ["delete"]
    match "/blog_rss_feeds/:id/delete", :to => "blog_rss_feeds#destroy", :via => ["delete"]
    resources :categories
    match "/categories/:id/delete", :to => "categories#destroy", :via => ["delete"]
    resources :publishers
    match "/publishers/:id/delete", :to => "publishers#destroy", :via => ["delete"]
    resources :pages
    resources :news_updates
    match "/news_updates/:id/delete", :to => "news_updates#destroy", :via => ["delete"]
    resources :reports do
      collection do
        match 'search' => 'reports#search', :via => [:get], :as => :search
        match 'search_reports' => 'reports#search_reports', :via => [:post]
        post :edit_multiple
        put :update_multiple
        post :upload_multiple
        post :save_position
        post :update_sheet
      end 

    end

    match "report_labels" =>'reports#report_labels', :via => [:get, :post]
    match "deactive_reports" =>'reports#deactive_reports', :via => [:get, :post]
    match "upload_files" => 'reports#upload_files', :via => [:get, :post]
    match "sort_reports" =>'reports#sort_reports', :via => [:get, :post]
   

    match "/reports/:id/delete", :to => "reports#destroy", :via => ["delete"]
    resources :messengers
    resources :conversations, only: [:show,:create] do
       resources :messengers
    end
    resources :orders
    resources :subscribers, :only => [:index, :show]  do 
      collection do
        post :toggle_active
      end
    end
    resources :testimonials
    match "/testimonials/:id/delete", :to => "testimonials#destroy", :via => ["delete"]
    match "/newsletter_templates/:id/delete", :to => "newsletter_templates#destroy", :via => ["delete"]
    match "/subscribers/:id/delete", :to => "subscribers#destroy", :via => ["delete"]
    match "order_details" =>'orders#order_details', :via => [:get, :post]
    resources :enquiries do
      collection do
        match 'list'=>"enquiries#list" ,:via=>[:get,:post]
        match 'list/:date'=>"enquiries#list_by_date" ,:via=>[:get,:post]
        match 'list/:date'=>"enquiries#delete_list" ,:via=>[:get,:delete]
        post :edit_multiple
      end
    end
   
    match "/pages/:id/delete", :to => "pages#destroy", :via => ["delete"]
    match "/enquiries/:id/delete_contact_us", :to => "enquiries#delete_contact_us", :via => ["delete"]
    
    match "/enquiries/:id/delete", :to => "enquiries#destroy", :via => ["delete"]
    match "contact_us" =>'enquiries#contact_us', :via => [:get, :post]
    match "/admin_change_password" => "dashboard#change_password", :via => [:get, :post]


    resources :newsletter_templates do 
      collection do
        post :send_multiple
        post :delete_multiple
      end
    end

    resources :clients
    match "/clients/:id/delete", :to => "clients#destroy", :via => ["delete"]
  end
  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products
  match "/send_password" => "password_resets#send_password" , :via=>[:post]
  match "/update_password" => "password_resets#update" , via: [:patch]
  match "/upload_excelsheet" => "admin/reports#upload_multiple" , via: [:post]
  match "/upload_reports" => "admin/reports#upload_reports" , via: [:post,:get]

  match "/upload_update_reports" => "admin/reports#upload_update_reports" , via: [:post,:get]

  resources :reports, only: [:show,:index]  do
    collection do
       match 'search' => 'reports#search', :via => [:get, :post], :as => :search
       get :get_all_reports
       get 'currency_section'
       post :show_advanced_search
    end 
    get :reset_filterrific, on: :collection    
  end 
  match "/reports/:id/:slug", :to => "reports#show", :via => ["get"]

  resources :categories, only: [:show,:index]

  resources :regions, only: [:show,:index] do
    member do
      match "/country_wise/:country_id", :to => "regions#country_wise", :via => ["get"]
    end
    collection do
      get 'reset_filter'
    end
  end
  resources :publishers, only: [:show,:index] do 
    collection do
      get 'load_more_publisher'
    end
  end
  resources :messages, only: [:create]
  resource :shopping_cart, only: [:show,:create]
  resources :pages, :only => [:show]
  match "/remove_item" => 'shopping_carts#remove_item', :via => [:get, :post]
  post "reports/enquiry" => "reports#enquiry"
  post "reports/enquiry_link" => "reports#enquiry_link"



  get "/clear_cart" => 'shopping_carts#clear_cart'
  get "/load_add_to_cart" => "shopping_carts#load_add_to_cart"
  get "/load_place_order_items" => "shopping_carts#load_place_order_items"

  post "/add_to_cart" => "shopping_carts#add_to_cart"
  

  post "blogs/post_comment" => "blogs#post_comment"

  get "/manage_address"  => 'shopping_carts#manage_address'
  get "/checkout"  => 'shopping_carts#checkout'
  get "/place_order"=> 'shopping_carts#place_order'
  get "/paypal_order_cancel"=> 'shopping_carts#paypal_order_cancel'
  get "/buynow_paypal_cancel"=> 'shopping_carts#buynow_paypal_cancel'
  resources :customers
  match "/update_quantity" => "shopping_carts#update_quantity", via: "post"
  match "/update_user" => "users#update_user", via: "post"
  match "/change_password" => "users#change_password", :via => [:get, :post]
  match "/update_password" => "users#update_password", :via => [:get, :post]
  match "/create_user" => "shopping_carts#create_user", via: "post"
  match "/signin_user" => "shopping_carts#signin_user", via: "post"
  match "/create_customer" => "shopping_carts#create_customer", via: "post"
  match "/complete_order" => "orders#complete_order", via: "post"
  match "/complete_ccavenue_order" => "orders#complete_ccavenue_order", :via => [:get, :post]

  match "/ccavenue_transfer" => "orders#ccavenue_transfer" , via: "post"
  match "/new_user_ccavenue_transfer" => "orders#new_user_ccavenue_transfer" , via: "post"
  
  match "/buynow_ccavenue_transfer" => "orders#buynow_ccavenue_transfer" , via: "post"


  get "purchase_complete" => "orders#purchase_complete"
  get "purchase_cancel" => "orders#purchase_cancel"
  
  get "checkout_complete" => "orders#checkout_complete"
  
  match "/express_checkout" => "orders#express_checkout", via: "post"
  match "/pay_checkout" => "orders#pay_checkout", via: "post"

  match "/wire_transfer" => "orders#wire_transfer", via: "post"
  get "/charges" , :controller=>"charges", :action=>"create"
  match "/save_chat_user" => "messages#save_chat_user", via: "post"
  resources :orders do
    collection do
      post 'send_to_ccavenue'
    end
  end
  resources :charges, only: [:new,:create]
  resources :continents, :only => [:index, :show] do
    collection do
      get 'global'
      get 'reset_filter_global'
      get 'reset_filter_continent'
    end
  end
  resources :countries, :only => [:index, :show] do
    collection do
      get 'reset_filter_country'
    end
  end
  match "/contact_us"=> "pages#contact_us", :via => [:get, :post], :path => '/contact-us'
  # match "/countries"=> "reports#countries", via: "get"
  # match "/continents"=> "reports#continent", via: "get"
  match "/save_contact_us" => "pages#save_contact_us", via: "post"
  match "/order_details" => "orders#order_details", :via => [:get, :post]
  match "/report_list" => "reports#continent_wise_reports", :via => [:get, :post]
  # match "/continents/global" => "continents#global", :via => [:get, :post]
  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end
  # get 'message/index'
  post "/email_report" => "reports#email_report"
  post "reports/subscriber" => "reports#subscriber"

  get "/allow" => "blogs#allow"
  get "/disallow" => "blogs#disallow"
  # match "/order_form" => "reports#order_form", :via => [:get, :post]
  # match "/report_info" => "reports#report_info", :via => [:get, :post]
  # match "/order_form" => "reports#generatePdf", :via => [:get, :post]
  
  # get 'messages/new'

   # post 'messages/new'
   # get 'messages/new'
   # post 'messages/create'
  resources :conversations, only: [:show,:create] do
    resources :messages, only: [:create]
  end

  match "/wiretransfer_response" => "shopping_carts#wiretransfer", :via => [:get, :post]
  match "/paypal_purchase_complete" => "orders#paypal_purchase_complete", :via => [:get, :post]


  match "/hdfc/response" => "orders#hdfc_buynow_complete", :via => [:get, :post]
  
  match "/hdfc_complete" => "orders#hdfc_complete", :via => [:get, :post]

  match "/hdfc/cart_response" => "orders#hdfc_cart_complete", :via => [:get, :post]
  

  get "zohoverify/verifyforzoho.html" => "home#verifyforzoho" 
  get "order_page" => "pages#order_page" 
  get "load_tweets" => "home#load_tweets" 


  resources :authorizations, only: [:destroy,:create]
  match '/auth/:provider/callback' => 'authorizations#create', :via => [:get, :post]
  match '/auth/failure' => 'authorizations#failure', :via => [:get, :post]
  match '/auth/:provider' => 'authorizations#blank', :via => [:get, :post]

  get "expire_cache" => "reports#expire_cache" 



  # get "zohoverify/404.html"
  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  post "shopping_carts/get_promo_code" => "shopping_carts#get_promo_code"
  post "shopping_carts/get_promo_code_specific" => "shopping_carts#get_promo_code_specific"

  get '/robots.txt' => 'home#robots'
  get '/sitemap.xml' => 'home#sitemap'
  get '/urllist.xml' => 'home#urllist'

  get '/sitemap' => 'home#site_map'

  get '/resubscribe'=> 'home#resubscribe'
  get '/unsubscribe_page'=> 'home#unsubscribe_page'
  get "/block_invitation"=> 'home#block_invitation'
  post "unsubscribe" => "home#unsubscribe"
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  match "/invoice_form" => "orders#invoice_form", :via => [:get, :post]
  
  get "/sample_request/:id" => "reports#sample_request" 
  get "/enquiry/:id" => "reports#any_question"
  get "/check_discount/:id" => "reports#check_discount"
  match '/rebuild_cache' => 'reports#rebuild_cache', via: [:delete]
  match "/login_user" => "users#check_login", via: [:post]
  match "/register_user" => "users#create_user", via: [:post]

  match "/order_form" => "reports#order_form", :via => [:get, :post]
  match "/report_info" => "reports#report_info", :via => [:get, :post]


  match "/advance_search" => "reports#advance_search", :via => [:get, :post]

  match "/vendor-information" =>'pages#vender_info', :via => [:get, :post]
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end


  get '404', :to => 'application#page_not_found'
  get '500', :to => 'application#internal_server_error'
end
