preload_app true
# Set the working application directory
# working_directory "/path/to/your/app"
working_directory "/home/ubuntu/www/wiseguy_optimized"

# Unicorn PID file location
# pid "/path/to/pids/unicorn.pid"
pid "/home/ubuntu/www/wiseguy_optimized/pids/unicorn.pid"

# Path to logs
# stderr_path "/path/to/log/unicorn.log"
# stdout_path "/path/to/log/unicorn.log"
stderr_path "/home/ubuntu/www/wiseguy_optimized/log/unicorn.log"
stdout_path "/home/ubuntu/www/wiseguy_optimized/log/unicorn.log"

# Unicorn socket
#listen "/tmp/unicorn.wiseguyreports.sock"
listen "/tmp/unicorn.wiseguy_optimized.sock"

# Number of processes
# worker_processes 4
worker_processes 4


# Time-out
timeout 1200

before_fork do |server, worker|
  defined?(ActiveRecord::Base) && ActiveRecord::Base.connection.disconnect!

  old_pid = "/home/ubuntu/www/wiseguy_optimized/pids/unicorn.pid"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      Process.kill("QUIT", File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      puts "Old master alerady dead"
    end
  end
end

after_fork do |server, worker|
  defined?(ActiveRecord::Base) && ActiveRecord::Base.establish_connection
  child_pid = server.config[:pid].sub('.pid', ".#{worker.nr}.pid")
  system("echo #{Process.pid} > #{child_pid}")
end

