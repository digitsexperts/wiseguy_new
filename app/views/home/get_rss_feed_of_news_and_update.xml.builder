
xml.instruct! :xml, :version => "1.0" 
xml.rss :version => "2.0" do
  xml.channel do
    # xml.title "New & Update"
    # xml.description "New & Update Description"
    # xml.pubDate "New & Update Date"
    # xml.categoryId "New & Update Category Id"
    # xml.categoryName "New & Update Category Name"
    # xml.link rss_feeds_url
    
    for news_update in @news_updates
      xml.item do
        xml.title news_update.title
        xml.description :type => "html" do
          xml.cdata!(news_update.get_description)
        end
        xml.pubDate news_update.rss_publish_date
        xml.categoryId news_update.category_id
        xml.link request.protocol + request.host_with_port+news_update_path(news_update)
        xml.guid request.protocol + request.host_with_port+news_update_path(news_update)
      end
    end
    unless @news_updates.present?
      xml.title "No feed items found"
    end
  end
end