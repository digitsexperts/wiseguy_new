$.fn.pressEnter = function(fn) {  
  
    return this.each(function() {  
        $(this).bind('enterPress', fn);
        $(this).keyup(function(e){
            if(e.keyCode == 13)
            {
              $(this).trigger("enterPress");
            }
        })
    });  
 };

function getUrlParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
} 

function search(){
  var search_object = {term: $("#search_query").val(), price: $("#price").val(), publisher: $("#publisher").val(), sorted_by: $("#sorted_by").val(), page: getUrlParameter('page')}
  if(!(search_object.term == "" && search_object.price == "" && search_object.publisher == "" && search_object.sorted_by == "")){
    $.ajax({
  	url: "/reports/search.js",
  	data: search_object,
  	success: function(search_result){
        $("#filterrific_results").replaceWith(search_result);
  	},
  	complete: function(search_result){
        $("#filterrific_results").replaceWith(search_result.responseText);
  	}
    })
  }
}

$(document).ready(function(){
  $("#search_query").pressEnter(function(){
    search();
  })

  $("#price").change(function(){
  	search();
  })

  $("#publisher").change(function(){
  	search();
  })

  $("#sorted_by").change(function(){
  	search();
  })
})

function showCategory(category){

}