class ReportsController < ApplicationController
  # before_action :check_show_by_id, only: [:show]
  require 'RMagick'
  include Magick
  include ActionView::Helpers::TextHelper
  def index
    @reports = Report.search_report(params[:q],params[:page])
  end

  def search
    fields = if (params["term"].present? && params["publisher"].present?)
               "@(title,publisher_name) '#{params['term']}','#{params['publisher']}'"
             elsif (params["term"].present? && params["publisher"].blank?)
               "@(title) '#{params['term']}'"
             elsif (params["term"].blank? && params["publisher"].present?)
               "@(publisher_name) '#{params['publisher']}'"
             else
               nil
             end

    filter = {
              :order => "#{params[:sorted_by]}", 
              :page => (request.format.symbol == :js) ? nil : params[:page], 
              :limit => 10
             }

    filter[:with] = {:one_user => eval(params[:price])} if params[:price].present?
    @reports = Report.search(fields,filter)
    #get_all_categories(nil,nil,nil,nil,nil)
    
    respond_to do |format|
      format.html {
        @categories = Category.get_all_categories
        render :template => "reports/index"
      }
      format.js { render(partial: 'reports/list') }
    end
  end


  def show
    
    if params[:id].is_number?
      @report = Report.find(params[:id])
    else
      reports = Report.search("@(slug,old_slug) '^#{Riddle::Query.escape(params[:id])}' | ('^#{Riddle::Query.escape(params[:id])}' | '^#{Riddle::Query.escape(params[:id])}*')",:limit => 5)
      reports.each do |report|
        @report = report if (report.slug == params[:id] || report.old_slug == params[:id])
      end
      @report = reports.first unless @report  
    end  

    @enquiry = Enquiry.new
    
    @related_report = Report.related_report(@report.category_id)
    @rates = @report.rates

    respond_to do |format|
      format.html
      format.js
      format.json { render json: @report.to_json }
    end
  end

 def enquiry
    @enquiry = Enquiry.new(enquiry_params)
    if @enquiry.save
      report = Report.find(@enquiry.report_id)
      enquiry_attributes = @enquiry.attributes.select do |attr, value|
        Enquiry.column_names.include?(attr.to_s)
      end
      report_attributes = report.attributes.select do |attr, value|
        Report.column_names.include?(attr.to_s)
      end
      json_data = enquiry_attributes.merge(report_attributes).merge(:enquiry_country_id => @enquiry.country_id, :report_publisher => Report.publisher_name_image_hash[report.publisher_id][:name], :report_category => report.category.name).to_json
      begin
        uri = URI.parse("http://crm.wiseguyreports.com/")
        http = Net::HTTP.new(uri.host, uri.port)
        request = Net::HTTP::Post.new("/CRM/v1/CreateLead")
        request.add_field('Content-Type', 'application/json')
        request.body = json_data
        response = http.request(request)
      rescue
        puts "Error communicating with crm..."
      end
      
      success = "Thank you, Your enquiry has been send"

      #UserMailer.delay(run_at: Time.now).send_enquiry(report, @enquiry)
      UserMailer.send_enquiry(report, @enquiry).deliver
      render :json => {notice: success} 
    else

      render :json => {error: "Please enter an correct capcha"} 

    end
  end

  def advance_search
    @regions = Region.all_regions
    @all_countries = Country.all
    @publishers = Publisher.publisher_list
    @categories = Category.get_all_categories
  end

  def show_advanced_search
    @reports  = Report.advanced_search_reports(params)
  end

  def email_report
    #UserMailer.delay(run_at: Time.now).email_reports(params[:email_report])
   # UserMailer.delay(run_at: Time.now).email_report_to_team(params[:email_report])
    UserMailer.email_reports(params[:email_report]).deliver
    UserMailer.email_report_to_team(params[:email_report]).deliver
    notice = "Thank you, Your message has been sent"
    render :json => {notice: notice}
    
  end

  def sample_request
    @enquiry = Enquiry.new
    reports = Report.search("@(slug,old_slug) '^#{Riddle::Query.escape(params[:id])}' | ('^#{Riddle::Query.escape(params[:id])}' | '^#{Riddle::Query.escape(params[:id])}*')",:limit => 5)
    reports.each do |report|
      @report = report if (report.slug == params[:id] || report.old_slug == params[:id])
    end
    @report = reports.first unless @report
    @countries = Country.get_all_countries
  end

  def check_discount
    @enquiry = Enquiry.new
    reports = Report.search("@(slug,old_slug) '^#{Riddle::Query.escape(params[:id])}' | ('^#{Riddle::Query.escape(params[:id])}' | '^#{Riddle::Query.escape(params[:id])}*')",:limit => 5)
    reports.each do |report|
      @report = report if (report.slug == params[:id] || report.old_slug == params[:id])
    end
    @report = reports.first unless @report
    @countries = Country.get_all_countries
  end

  def any_question
    @enquiry = Enquiry.new
    reports = Report.search("@(slug,old_slug) '^#{Riddle::Query.escape(params[:id])}' | ('^#{Riddle::Query.escape(params[:id])}' | '^#{Riddle::Query.escape(params[:id])}*')",:limit => 5)
    reports.each do |report|
      @report = report if (report.slug == params[:id] || report.old_slug == params[:id])
    end
    @report = reports.first unless @report
    @countries = Country.get_all_countries
  end
  
  def subscriber
    subscriber = Subscriber.search("@(email) #{Riddle::Query.escape(params[:subscriber][:email])}")
    if subscriber.blank?
      Subscriber.create(email: params[:subscriber][:email], status: true)
      UserMailer.new_subscription_to_team(params[:subscriber][:email]).deliver
      flash[:success] = "Thank you for subscribing to Wiseguy Reports Newsletter. We are delighted to have you !! "
      redirect_to root_path
    elsif subscriber.first.status
      flash[:success] = "You are aleady subscribed to Wiseguy Reports Newsletter."
      redirect_to root_path
    else
      subscriber.first.update_attributes(status: true)
      UserMailer.new_subscription_to_team(params[:subscriber][:email]).deliver
      flash[:success] = "Thank you for subscribing to Wiseguy Reports Newsletter. We are delighted to have you !! "
      redirect_to root_path
    end
  end

  def rebuild_cache
    User.build_cache
    Report.clear_fragment_cache

    `rake cache_builder:create_fragment_cache`
    render :nothing => true
  end

  def order_form

    @report = Report.find(params[:report_id])
    respond_to do |format|
      format.html # show.html.erb
      format.pdf do
        render :pdf => "order_form", encoding: 'utf8'
      end
    end
  end

  def report_info
    @report = Report.find(params[:report_id])

    respond_to do |format|
      format.html do
        render :layout => false
      end
      format.pdf do
        render :pdf => "report_info", encoding: 'utf8'
      end
    end
  end
  

  private

  def subscriber_params
    params.require(:subscriber).permit(:email,:status)
  end

  def enquiry_params
    params.require(:enquiry).permit(:first_name,:last_name,:email,:job_title, :phone_no, :company, :interest_area, :enquiry, :message,:enquiry_type,:report_id,:country_id)
  end

  def check_show_by_id
    if params[:id].is_number?
      redirect_to :action => :show,:id => Report.find(params[:id]).slug
    end
  end
end
