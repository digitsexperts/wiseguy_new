class CategoriesController < ApplicationController

  def index
  end

  def show
  	@category = Category.friendly.find(params[:id])
    get_reports_list
    
    respond_to do |format|
      format.html
      format.js
    end
  end


  def get_reports_list
    category_ids = []
    category_ids << @category.id
    category_ids << Category.sphinx_get_child_ids([@category.id])
    category_ids = category_ids.flatten
    page = params[:page].present? ? params[:page] : 1

    @reports = Report.search(:sql => {:joins => [:publisher], :select => "reports.id, reports.title, reports.day, reports.publish_month, reports.slug, reports.description, reports.one_user, reports.report_image as rimage, publishers.report_image as pimage, publishers.name as publisher_name,reports.publisher_id,reports.category_id"},:with=>{category_id: category_ids,:upcoming => 0},:order=>"publish_date desc,one_user desc",:page => page, :per_page => 10)
    
    #get_all_categories(@category.root.id,nil,nil,nil,nil)

    @selected_category_id = [@category.root.id]
    @contain_global = false
  end

  def reset_to_default_by_category
    @category = Category.friendly.find(params[:id])
    get_reports_list
    respond_to do |format|
      format.js
    end
  end
end
