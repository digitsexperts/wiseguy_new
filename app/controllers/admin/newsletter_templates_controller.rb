class Admin::NewsletterTemplatesController < Admin::BaseController

  before_action :set_template, only: [:show, :edit, :update, :destroy]

  def index
  	@templates  = NewsletterTemplate.select("id","subject","update_date")
  end

  def new
    @template = NewsletterTemplate.new

  end

  def create
    @template = NewsletterTemplate.new(template_params)
    if @template.save
      flash[:success] = "Template created successfully"
      redirect_to admin_newsletter_templates_path
    else
      render :new
    end
  end

  def edit
   
  end

  def update
    if @template.update_attributes(template_params)
      flash[:success] = "Template saved successfully"
      redirect_to admin_newsletter_templates_path
    else
      render :new
    end
  end


  def destroy
    if @template.destroy
      flash[:success] = "Template deleted successfully"
      redirect_to admin_newsletter_templates_path
    end 
  end


  def show
    
  end

  def send_multiple

   if params[:message] == "send_all"
     subscribers = Subscriber.search(:conditions=>{:subscriber_status => 1 })
   else
     subscribers = Subscriber.search(:conditions=>{:subscriber_status => 1 },:with=>{:subscriber_id=>params[:subscriber_ids]})
   end
   
   #subscribers.each{ |recipient| UserMailer.delay(run_at: Time.now).send_newsletter(recipient) }
   background do
     subscribers.each{ |recipient| UserMailer.send_newsletter(recipient).deliver }
   end
   render :json=>{:status=>200}
    
  end
  
  def delete_multiple
    Subscriber.delete_all(id: params[:id])
    render :json=>{:status=>200}
  end

  private

  def set_template
    @template = NewsletterTemplate.find(params[:id])
  end

  def template_params
    params.require(:newsletter_template).permit(:title,:header, :body, :footer,:subject,:update_date)
  end
end
