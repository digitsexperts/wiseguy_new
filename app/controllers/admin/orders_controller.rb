class Admin::OrdersController < Admin::BaseController

	
  def index	
  	@orders = Order.paginate(:page => params[:page], :per_page => 10)
  end

  def order_details
  	@order_details = OrderDetail.where(order_id: params[:order_id])
  end
end
