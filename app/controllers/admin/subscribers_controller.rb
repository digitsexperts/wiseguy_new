class Admin::SubscribersController < Admin::BaseController
  before_action :set_subscriber, only: [:show, :toggle_active, :destroy]
  
  def index
    if params[:status].present?
      @subscribers = Subscriber.where(:status=> eval(params[:status])).order("id desc").paginate(:page => params[:page], :per_page => 20)
    else
      @subscribers = Subscriber.order("id desc").paginate(:page => params[:page], :per_page => 20)
    end
  end

  def show
  end

  def destroy
  	 if @subscriber.destroy
       flash[:notice] = "subscriber deleted successfully"
       redirect_to admin_subscribers_path
     end 
  end

  def toggle_active
   
    @subscriber.update_attributes(status: params[:completed])
    render :json=> true
  end

  private

  def set_subscriber
    @subscriber = Subscriber.find(params[:id])
  end

end
