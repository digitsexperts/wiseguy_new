class Admin::PublishersController < Admin::BaseController
  before_action :set_publisher, only: [:show, :edit, :update, :destroy]
  before_filter :correct_user

  def index
    @publishers = Publisher.order("name asc").paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @publisher = Publisher.new
  end

  def create
    
    @publisher = Publisher.new(publisher_params)
    if @publisher.save
      ActionController::Base.new.expire_fragment(/admin-publisher-list/)
      ActionController::Base.new.expire_fragment(/allpublishers/)
      ActionController::Base.new.expire_fragment(/publisher_name_images/)
      flash[:success] = "Publisher created successfully"
      redirect_to admin_publishers_path
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @publisher.update_attributes(publisher_params)
      ActionController::Base.new.expire_fragment(/admin-publisher-list/)
      ActionController::Base.new.expire_fragment(/allpublishers/)
      ActionController::Base.new.expire_fragment(/publisher_name_images/)
      flash[:success] = "Publisher saved successfully"
      redirect_to admin_publishers_path
    else
      render :new
    end
  end


  def destroy
    if @publisher.destroy
      ActionController::Base.new.expire_fragment(/admin-publisher-list/)
      ActionController::Base.new.expire_fragment(/allpublishers/)
      ActionController::Base.new.expire_fragment(/publisher_name_images/)
      flash[:success] = "Publisher deleted successfully"
      redirect_to admin_publishers_path
    end 
  end



  def show
  end


  private

  def correct_user
    redirect_to "/admin/dashboard", :flash => { :error => "Access denied!" }  if current_user.is_blog_admin?
  end

  def set_publisher
    @publisher = Publisher.friendly.find(params[:id])
  end

  def publisher_params
    params.require(:publisher).permit(:name,:description,:logo,:report_image,:report_background,:text_position,:text_color,:active, :sample_report_flag,:report_price_flag)
  end
end
