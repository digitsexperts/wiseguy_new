class Admin::NewsUpdatesController < Admin::BaseController
	before_action :set_news_update, only: [:show, :edit, :update, :destroy]


  def index
    @news_updates = NewsUpdate.paginate(:page => params[:page], :per_page => 10)
  end

  def new
    @news_update = NewsUpdate.new
    @categories = Category.all
  end

  def create
    @news_update = NewsUpdate.new(news_update_params)
    if @news_update.save
      flash[:success] = "News created successfully"
      redirect_to admin_news_updates_path
    else
      render :new
    end
  end

  def edit
    @categories = Category.all
  end

  def update
    if @news_update.update_attributes(news_update_params)
      flash[:success] = "News saved successfully"
      redirect_to admin_news_updates_path
    else
      render :new
    end
  end


  def destroy
    if @news_update.destroy
      flash[:success] = "News deleted successfully"
      redirect_to admin_news_updates_path
    end 
  end


  def show
    
  end

  private

  def set_news_update
    @news_update = NewsUpdate.friendly.find(params[:id])
  end

  def news_update_params
    params.require(:news_update).permit(:title,:description,:date,:news_image,:category_id)
  end
end
