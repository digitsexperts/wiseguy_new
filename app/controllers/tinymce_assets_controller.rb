class TinymceAssetsController < ApplicationController
  def create
    info_graphics = InfoGraphics.create(report_image: params[:file])
    render json: {
      image: {
        url: info_graphics.report_image_url
      }
    }, layout: false, content_type: "text/html"
  end	
end
