class ShoppingCartItem < ActiveRecord::Base
  acts_as_shopping_cart_item

  def currency
  	self.owner.currency
  end
end
