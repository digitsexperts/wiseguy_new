module ClearBlogCache
  extend ActiveSupport::Concern

  included do
    after_save :clear_cache
    after_update :clear_cache
    after_destroy :clear_cache
  end

  def clear_cache
    ActionController::Base.new.expire_fragment("all_blog_categories",options = nil)  
    self.tags.each do |tag|
    	ActionController::Base.new.expire_fragment("get-taged-blogs-#{tag.name}",options = nil) 	
    end 
    ActionController::Base.new.expire_fragment("get-category-blogs-#{self.category.name}",options = nil)   
    ActionController::Base.new.expire_fragment("get-blog-archive",options = nil)      
  end
end