class ShoppingCart < ActiveRecord::Base
  acts_as_shopping_cart

  has_many   :orders,   :foreign_key => :cart_id
  belongs_to :user
 


  def add(object, price, quantity = 1, cumulative = true,purchase_type)
    cart_item = item_for(object)
    shopping_cart_items.create(:item => object, :price => price, :quantity => quantity,:purchase_type => purchase_type)
  end

  def total
    self.shopping_cart_items.collect {|item| item.discounted_price.nil? ? (item.quantity*item.price) : (item.quantity*item.discounted_price) }.sum
  end

  def create_order(ip_address,user,payment_method_type,order_id)
    require 'net/http'
    report_arr = []
    user_order = user.orders.create(:order_status=>'PENDING', :ip_address => ip_address, :order_total => self.total,:payment_method_type => payment_method_type)
    json_data = user.to_json(:include => [:customer => {:include => [:country]}])
    begin
      uri = URI.parse("http://crm.wiseguyreports.com/")
      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new("/CRM/v1/CreateContact")
      request.add_field('Content-Type', 'application/json')
      request.body = json_data
      response = http.request(request)
    rescue
      puts "Error communicating with crm..."
    end
    self.shopping_cart_items.each do |item|
      if OrderDetail.exists?(report_id: item.item_id, order_id: user_order.id)        
        order_detail = OrderDetail.where(order_details_status: 'PENDING',report_id: item.item_id, order_id: user_order.id).first
        order_detail.update_attribute("quantity", (order_detail.quantity+item.quantity))
      else
        order_detail=OrderDetail.create(order_details_status: 'PENDING', report_id: item.item_id, report_value: item.price,
          quantity: item.quantity, order_id: user_order.id)
      end
      @report = Report.find(item.item_id)
      @report.increment!(:selling_count) 
       
    end
    self.shopping_cart_items.each do |i|
      report_arr << i.item_id
    end
  end


  def self.load(shopping_cart_id)
    shopping_cart_id = create.id unless shopping_cart_id

      JSON.parse(find(shopping_cart_id).to_json(
        :only => [:id,:currency],
        :include => {
                      :shopping_cart_items => 
                      {:only => [:id,:item_id,:quantity,:purchase_type,:price,:discounted_price,:discount], 
                        :include => 
                        {:item => 
                          { :only => [:id,:title,:report_image, :slug, :day, :publish_month],
                            :include => {
                                          :publisher => {:only => [:name,:report_image]
                                                        },
                                          :category => {:only => [:id]
                                                        }              
                                        }
                          }
                        },
                        :methods => [:currency]
                      }
                    },
                    :methods => [:total,:subtotal]                          )
                )

  end



end
