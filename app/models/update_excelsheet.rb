class UpdateExcelsheet < ActiveRecord::Base
	mount_uploader :file, UpdateExcelsheetUploader
    enum status: [ :uploaded, :dumped ]

	def self.update_excelsheets
	  require 'csv'
      require 'net/http'
      UpdateExcelsheet.uploaded.limit(1).each do |excelsheet|
      	excelsheet_file_path, csv_file_path, file_name = UpdateExcelsheet.create_folder_structure(excelsheet)
	       begin
		      puts "Downloading Excelsheet-#{excelsheet.id}"
		      system("wget #{excelsheet.file.url} -O #{excelsheet_file_path}")
		    rescue Exception => e
		      # excelsheet.failed!
		    # UserMailer.excelsheet_download_fail(excelsheet)
		    next
	      end
		    if File.zero?(excelsheet_file_path)
	        # excelsheet.failed!
	        # UserMailer.excelsheet_download_fail(excelsheet)
	        next
	      end
	      begin
	        puts "Converting Excelsheet-#{excelsheet.id} to csv"
	        system("ssconvert #{excelsheet_file_path} '#{csv_file_path}'")
	      rescue Exception => e
	        # excelsheet.failed!
	        # UserMailer.invalid_excelsheet(excelsheet)
	        next
	      end

	      unless File.exist?(csv_file_path)
	        excelsheet.failed!
	        # UserMailer.unable_to_convert_excelsheet(excelsheet)
	        next
	      end
	      
	      header = `head -n 1 #{csv_file_path}`.chomp
	      `sed -i '1d' #{csv_file_path}`
	      
	      `sed -i 's/_x000D_//g' #{csv_file_path}`
	      `sed -i 's/_x000d_//g' #{csv_file_path}`
	      #check for valid headers
	      # header_analysis = valid_header(excelsheet,header.split(","))
	      # if !header_analysis[0]
	      #    # UserMailer.invalid_excelsheet_columns(excelsheet,header_analysis[1])
	      #    excelsheet.failed!
	      #    next
	      # end
	      dump_csvs(csv_file_path,excelsheet)
	      
	      excelsheet.update_columns(:status => 1)
	      puts "Finished processing Excelsheet-#{excelsheet.id}"
	      output = `RAILS_ENV=production rake ts:index`
	      puts output
	      puts "Finished indexing, clearing cache at #{DateTime.now}"
	      Rails.cache.clear
	      ActiveRecord::Base.connection.close
      end  
	end





	 def self.dump_csvs(csv_file_path,excelsheet)
	   rows = CSV.readlines(csv_file_path)
	   publisher_id = Publisher.where(name: "GlobalData").first.id

	   rows.each do |row|
	   	
	     begin
		    Report.where("publisher_id= ? AND title=?",publisher_id, row[0]).delete_all
		 rescue Exception => e
		   next
		 end
	   end
	   ActiveRecord::Base.connection.reconnect!
	 end









  def UpdateExcelsheet.create_folder_structure(excelsheet)
    FileUtils.rm_rf("#{Rails.root}/public/update_sheet/#{excelsheet.id}/") if Dir.exists?("#{Rails.root}/public/update_sheet/#{excelsheet.id}/")
    FileUtils.rm_rf("#{Rails.root}/public/update_sheet/") if Dir.exists?("#{Rails.root}/public/update_sheet/")
    Dir.mkdir("#{Rails.root}/public/update_sheet/")
    Dir.mkdir("#{Rails.root}/public/update_sheet/#{excelsheet.id}/")
    Dir.mkdir("#{Rails.root}/public/update_sheet/#{excelsheet.id}/split")
    Dir.mkdir("#{Rails.root}/public/update_sheet/#{excelsheet.id}/failed_data")
    Dir.mkdir("#{Rails.root}/public/update_sheet/#{excelsheet.id}/succeeded_data")
    Dir.mkdir("#{Rails.root}/public/update_sheet/#{excelsheet.id}/sql_files")
    [
      "#{Rails.root}/public/update_sheet/#{excelsheet.id}/#{excelsheet.file.file.filename}",
      "#{Rails.root}/public/update_sheet/#{excelsheet.id}/#{excelsheet.file.file.filename.split(".")[0]}.csv",
       excelsheet.file.file.filename.split(".")[0]
    ]
  end


end
