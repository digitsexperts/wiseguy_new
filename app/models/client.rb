class Client < ActiveRecord::Base

  mount_uploader :client_logo, ClientLogoUploader

  extend FriendlyId
  friendly_id :name, use: :slugged
end
