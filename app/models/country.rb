class Country < ActiveRecord::Base
	# serialize :region_ids, Array
  USA_ID    = 214
  CANADA_ID = 35
  UK_ID         = 213
  AUSTRALIA_ID  = 12
  
  has_and_belongs_to_many :regions

  has_many :customers , :dependent => :destroy
  #has_and_belongs_to_many :country_reports, :class_name => "Report"
  has_many :country_reports
  has_many :reports, through: :country_reports
  belongs_to :continent

  # has_and_belongs_to_many :regions
  has_and_belongs_to_many :region_reports, :class_name => "Report"
  has_many :categories, :through=> :country_reports
  extend FriendlyId
  friendly_id :abbreviation

  ACTIVE_COUNTRY_IDS = [ USA_ID ]
  DROPDOWN_COUNTRY_IDS = [ USA_ID, CANADA_ID, UK_ID, AUSTRALIA_ID ]

  def self.find_by_name(country_name)
    Country.find(country_name).name
  end

  def continent_name
    self.continent.name rescue nil
  end

  def region_names
    self.regions.pluck(:name).join(" , ")
  end

  # Call this method to display the country_abbreviation - country with and appending name
  #
  # @example abbreviation == USA, country == 'United States'
  #   country.abbreviation_name(': capitalist') => 'USA - United States : capitalist'
  #
  # @param [append name, optional]
  # @return [String] country abbreviation - country name
  def abbreviation_name(append_name = "")
    ([abbreviation, name].join(" - ") + " #{append_name}").strip
  end

  # Call this method to display the country_abbreviation - country
  #
  # @example abbreviation == USA, country == 'United States'
  #   country.abbrev_and_name => 'USA - United States'
  #
  # @param none
  # @return [String] country abbreviation - country name
  def abbrev_and_name
    abbreviation_name
  end

  def self.get_all_countries
    Rails.cache.fetch("all-countries") do
      self.all
    end
  end

  def self.list
    Rails.cache.fetch("country-list") do
      JSON.parse(Country.select("id","name","lower(abbreviation) as abbreviation","reports_count").order("name asc").to_json)
    end
  end

  def self.grouped_by_letters
    grouped = {}
    list.each do |country|
      letter = country['name'].slice(0,1).upcase
      grouped[letter] ||= []
      grouped[letter] << country
    end
    grouped
  end

  def self.recount_report_count
    all.each do |country|
      new_reports_count = Report.search(:sql => {:select => 'reports.id'}, :with =>  {:upcoming => 0,:country_id => country.id}).count
      country.update_attribute('reports_count',new_reports_count)
    end
  end
end
