class PurchaseInvoiceDetail < ActiveRecord::Base
  establish_connection :crm_database
  belongs_to  :purchase_invoice
end
