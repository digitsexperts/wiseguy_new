class Invoice < ActiveRecord::Base
  establish_connection :crm_database
  has_many :product_details
end
