class PromoCode < ActiveRecord::Base
    establish_connection :crm_database
    has_many :promo_code_for_reports, :dependent => :destroy
end
