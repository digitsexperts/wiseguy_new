class Subscriber < ActiveRecord::Base

   scope :active, -> { where("status = ?",true) }

	def self.send_newsletters
	  Subscriber.active.each{ |recipient| UserMailer.send_newsletter(recipient).deliver }
	end	
end
