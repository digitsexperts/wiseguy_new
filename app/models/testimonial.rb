class Testimonial < ActiveRecord::Base

	def self.list
      Rails.cache.fetch("testimonial-list") do
      	Testimonial.select("id","message","name","position").limit(10)
      end
	end

	
end
