class Contact < ActiveRecord::Base
  establish_connection :crm_database
  has_one :contact_address_detail
end
