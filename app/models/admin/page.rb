class Admin::Page < ActiveRecord::Base
  self.table_name = "admin_pages"
  extend FriendlyId
  friendly_id :title, use: :slugged

  def self.show(slug)
  	Rails.cache.fetch("page-#{slug}") do
  	  self.friendly.find(slug)
  	end
  end
end
