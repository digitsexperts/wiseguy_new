ThinkingSphinx::Index.define :publisher, :with => :active_record do
  indexes name, :sortable => true
  indexes report_image
  has reports.category_id, :as => :report_category_id
end