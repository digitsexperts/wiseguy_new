ThinkingSphinx::Index.define :subscriber, :with => :active_record do
  indexes status, :as => :subscriber_status
  indexes email
  has id, :as => :subscriber_id
end