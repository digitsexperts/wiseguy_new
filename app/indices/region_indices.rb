ThinkingSphinx::Index.define :region, :with => :active_record do
  has countries.id, :as => :countries_ids, :sortable => true
  has id, :as => :region_id
  has region_reports.id ,:as=> :report_id
  indexes name, :as=>:region_name, :sortable => true
end