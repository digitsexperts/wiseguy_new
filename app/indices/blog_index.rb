ThinkingSphinx::Index.define "admin/blog", :with => :active_record do
  indexes title, :sortable => true
end