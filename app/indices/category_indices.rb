ThinkingSphinx::Index.define :category, :with => :active_record do
  indexes ancestry, :sortable => true
  has id, :as => :category_id
  has publishers.id,:as => :publisher_id
  has parent_id
  indexes name, :as=>:category_name, :sortable => true
  has ancestry, :as=> :root_ancestry, :type => :integer
end